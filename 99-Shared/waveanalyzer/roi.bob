<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-07-11 15:57:07 by muyuanwang-->
<display version="2.0.0">
  <name>roi</name>
  <width>680</width>
  <height>1010</height>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-background</name>
    <width>680</width>
    <height>1010</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-background</name>
    <x>10</x>
    <y>10</y>
    <width>660</width>
    <height>490</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <corner_width>5</corner_width>
    <corner_height>5</corner_height>
  </widget>
  <widget type="xyplot" version="3.0.0">
    <name>X/Y Plot</name>
    <x>20</x>
    <y>20</y>
    <width>640</width>
    <height>400</height>
    <title>ROI - Stats</title>
    <title_font>
      <font family="Source Sans Pro" style="REGULAR" size="21.0">
      </font>
    </title_font>
    <show_toolbar>true</show_toolbar>
    <show_legend>false</show_legend>
    <x_axis>
      <title>f [kHz]</title>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
      <minimum>0.0</minimum>
      <maximum>10.0</maximum>
      <show_grid>true</show_grid>
      <title_font>
        <font family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </title_font>
      <scale_font>
        <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </scale_font>
      <visible>true</visible>
    </x_axis>
    <y_axes>
      <y_axis>
        <title>Mag [a.u.]</title>
        <autoscale>true</autoscale>
        <log_scale>true</log_scale>
        <minimum>1.0E-10</minimum>
        <maximum>1.0E-4</maximum>
        <show_grid>true</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <on_right>false</on_right>
        <visible>true</visible>
        <color>
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </color>
      </y_axis>
    </y_axes>
    <traces>
      <trace>
        <name></name>
        <x_pv>$(P)$(R)$(FFTWIST)-RoiFscale</x_pv>
        <y_pv>$(P)$(R)$(FFTWIST)-RoiMagn</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="0" green="0" blue="255">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
    </traces>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>peak mag.:</text>
    <x>400</x>
    <y>420</y>
    <width>120</width>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>frequency [peak]:</text>
    <x>400</x>
    <y>460</y>
    <width>120</width>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update</name>
    <pv_name>$(P)$(R)$(FFTWIST)-$(STAT)PeakMagn</pv_name>
    <x>530</x>
    <y>420</y>
    <width>120</width>
    <height>30</height>
    <format>2</format>
    <precision>2</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update</name>
    <pv_name>$(P)$(R)$(FFTWIST)-$(STAT)PeakFreq</pv_name>
    <x>530</x>
    <y>460</y>
    <width>120</width>
    <height>30</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>roi start:</text>
    <x>30</x>
    <y>420</y>
    <width>80</width>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update</name>
    <pv_name>$(P)$(R)$(FFTWIST)-$(ROI)Start_RBV</pv_name>
    <x>240</x>
    <y>420</y>
    <width>120</width>
    <height>30</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="textentry" version="3.0.0">
    <name>Text Entry</name>
    <pv_name>$(P)$(R)$(FFTWIST)-$(ROI)Start</pv_name>
    <x>120</x>
    <y>420</y>
    <height>30</height>
    <horizontal_alignment>1</horizontal_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>roi size:</text>
    <x>30</x>
    <y>460</y>
    <width>80</width>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update</name>
    <pv_name>$(P)$(R)$(FFTWIST)-$(ROI)Size_RBV</pv_name>
    <x>240</x>
    <y>460</y>
    <width>120</width>
    <height>30</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="textentry" version="3.0.0">
    <name>Text Entry</name>
    <pv_name>$(P)$(R)$(FFTWIST)-$(ROI)Size</pv_name>
    <x>120</x>
    <y>460</y>
    <height>30</height>
    <horizontal_alignment>1</horizontal_alignment>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-background</name>
    <x>10</x>
    <y>510</y>
    <width>660</width>
    <height>490</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <corner_width>5</corner_width>
    <corner_height>5</corner_height>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>step min:</text>
    <x>30</x>
    <y>960</y>
    <width>80</width>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>step max:</text>
    <x>230</x>
    <y>960</y>
    <width>80</width>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="textentry" version="3.0.0">
    <name>Text Entry</name>
    <pv_name>$(PM)$(R)Wa-DfDNXmin</pv_name>
    <x>120</x>
    <y>960</y>
    <height>30</height>
    <horizontal_alignment>1</horizontal_alignment>
  </widget>
  <widget type="textentry" version="3.0.0">
    <name>Text Entry</name>
    <pv_name>$(PM)$(R)Wa-DfDNXmax</pv_name>
    <x>320</x>
    <y>960</y>
    <height>30</height>
    <horizontal_alignment>1</horizontal_alignment>
  </widget>
  <widget type="bool_button" version="2.0.0">
    <name>Action Button</name>
    <pv_name>$(PM)$(R)Wa-Write</pv_name>
    <x>30</x>
    <y>920</y>
    <width>490</width>
    <off_label>Write</off_label>
    <on_label>Write</on_label>
    <show_led>false</show_led>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>write</description>
      </action>
    </actions>
    <tooltip>$(actions)</tooltip>
    <mode>1</mode>
    <show_confirm_dialog>false</show_confirm_dialog>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Text Update</name>
    <pv_name>$(PM)$(R)Wa-CavSensitivity</pv_name>
    <x>530</x>
    <y>960</y>
    <width>120</width>
    <height>30</height>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="xyplot" version="3.0.0">
    <name>X/Y Plot</name>
    <x>20</x>
    <y>520</y>
    <width>640</width>
    <height>400</height>
    <title>Cavity tuning sensitivity</title>
    <title_font>
      <font family="Source Sans Pro" style="REGULAR" size="21.0">
      </font>
    </title_font>
    <show_toolbar>true</show_toolbar>
    <show_legend>false</show_legend>
    <x_axis>
      <title>steps</title>
      <autoscale>false</autoscale>
      <log_scale>false</log_scale>
      <minimum>0.0</minimum>
      <maximum>250000.0</maximum>
      <show_grid>true</show_grid>
      <title_font>
        <font family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </title_font>
      <scale_font>
        <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </scale_font>
      <visible>true</visible>
    </x_axis>
    <y_axes>
      <y_axis>
        <title>f [kHz]</title>
        <autoscale>false</autoscale>
        <log_scale>false</log_scale>
        <minimum>704175.0</minimum>
        <maximum>704425.0</maximum>
        <show_grid>true</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <on_right>false</on_right>
        <visible>true</visible>
        <color>
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </color>
      </y_axis>
    </y_axes>
    <traces>
      <trace>
        <name></name>
        <x_pv>$(PM)$(R)Wa-Xarr</x_pv>
        <y_pv>$(PM)$(R)Wa-Yarr</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>0</trace_type>
        <color>
          <color red="0" green="0" blue="255">
          </color>
        </color>
        <line_width>1</line_width>
        <line_style>2</line_style>
        <point_type>2</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
    </traces>
  </widget>
  <widget type="combo" version="2.0.0">
    <name>Combo Box</name>
    <pv_name>$(PM)$(R)Wa-ArrayCtrl</pv_name>
    <x>530</x>
    <y>920</y>
    <width>120</width>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>sensitivity:</text>
    <x>440</x>
    <y>960</y>
    <width>80</width>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
</display>
