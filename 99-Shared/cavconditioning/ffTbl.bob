<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-10-11 11:08:17 by muyuanwang-->
<display version="2.0.0">
  <name>FF</name>
  <width>240</width>
  <height>360</height>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>Enable:</text>
    <x>10</x>
    <y>50</y>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="slide_button" version="2.0.0">
    <name>Slide Button</name>
    <pv_name>$(PD)$(LLRF)$(PI_TYPE)PulseGenEn</pv_name>
    <label></label>
    <x>120</x>
    <y>50</y>
    <width>110</width>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>Power level P:</text>
    <x>10</x>
    <y>130</y>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="spinner" version="2.0.0">
    <name>Spinner</name>
    <pv_name>$(PD)$(LLRF)$(PI_TYPE)PulseGenP</pv_name>
    <x>120</x>
    <y>130</y>
    <width>110</width>
    <height>30</height>
    <precision>2</precision>
    <show_units>true</show_units>
    <vertical_alignment>1</vertical_alignment>
    <scripts>
      <script file="EmbeddedPy" check_connections="false">
        <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
step = PVUtil.getDouble(pvs[0])
widget.setPropertyValue('increment',step)]]></text>
        <pv_name>$(PD)$(R)$(PI_TYPE)AStep.RVAL</pv_name>
      </script>
    </scripts>
    <maximum>1100.0</maximum>
    <increment>10.0</increment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>Fill ratio:</text>
    <x>10</x>
    <y>210</y>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="spinner" version="2.0.0">
    <name>Spinner</name>
    <pv_name>$(PD)$(LLRF)$(PI_TYPE)PulseGenPfillRatio</pv_name>
    <x>120</x>
    <y>210</y>
    <width>110</width>
    <height>30</height>
    <precision>2</precision>
    <vertical_alignment>1</vertical_alignment>
    <minimum>1.0</minimum>
    <maximum>4.0</maximum>
    <increment>0.1</increment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>Filling time:</text>
    <x>10</x>
    <y>250</y>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="spinner" version="2.0.0">
    <name>Spinner</name>
    <pv_name>$(PD)$(LLRF)$(PI_TYPE)PulseGenTfill</pv_name>
    <x>120</x>
    <y>250</y>
    <width>110</width>
    <height>30</height>
    <show_units>true</show_units>
    <vertical_alignment>1</vertical_alignment>
    <increment>0.01</increment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>Rise time:</text>
    <x>10</x>
    <y>290</y>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="spinner" version="2.0.0">
    <name>Spinner</name>
    <pv_name>$(PD)$(LLRF)$(PI_TYPE)PulseGenTslope</pv_name>
    <x>120</x>
    <y>290</y>
    <width>110</width>
    <height>30</height>
    <show_units>true</show_units>
    <vertical_alignment>1</vertical_alignment>
    <increment>0.01</increment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>Phase [deg]:</text>
    <x>10</x>
    <y>330</y>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="spinner" version="2.0.0">
    <name>Spinner</name>
    <pv_name>$(PD)$(LLRF)$(PI_TYPE)PulseGenPhase</pv_name>
    <x>120</x>
    <y>330</y>
    <width>110</width>
    <height>30</height>
    <precision>2</precision>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>HEADER2</class>
    <text>Feed-forward</text>
    <width>240</width>
    <height>40</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <background_color>
      <color red="188" green="173" blue="213">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <actions>
    </actions>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="combo" version="2.0.0">
    <name>Combo Box</name>
    <pv_name>$(PD)$(R)$(PI_TYPE)AStep</pv_name>
    <x>120</x>
    <y>170</y>
    <width>110</width>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>Step:</text>
    <x>10</x>
    <y>170</y>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>Fixed $(PI_TYPE):</text>
    <x>10</x>
    <y>90</y>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="slide_button" version="2.0.0">
    <name>Slide Button</name>
    <pv_name>$(PD)$(RD)RFCtrlCnst$(PI_TYPE)En</pv_name>
    <label></label>
    <x>120</x>
    <y>90</y>
    <width>110</width>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LED</name>
    <pv_name>$(PD)$(RD)RFCtrlCnst$(PI_TYPE)En-RB</pv_name>
    <x>190</x>
    <y>95</y>
  </widget>
</display>
