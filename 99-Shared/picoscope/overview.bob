<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2023-10-02 15:18:28 by muyuanwang-->
<display version="2.0.0">
  <name>Picoscope $(SYS)</name>
  <width>1100</width>
  <height>1120</height>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-background</name>
    <x>10</x>
    <y>60</y>
    <width>1080</width>
    <height>1060</height>
    <line_width>1</line_width>
    <line_color>
      <color red="175" green="175" blue="175">
      </color>
    </line_color>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
    <corner_width>3</corner_width>
    <corner_height>3</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Titlebar</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>1100</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <class>TITLE</class>
    <text>Picoscope $(SYS)</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>400</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="navtabs" version="2.0.0">
    <name>Navigation Tabs</name>
    <tabs>
      <tab>
        <name>Acquisition</name>
        <file>acquisition.bob</file>
        <macros>
        </macros>
        <group_name></group_name>
      </tab>
      <tab>
        <name>A (Trigger)</name>
        <file>channel.bob</file>
        <macros>
          <CH>A</CH>
          <UNIT>V</UNIT>
        </macros>
        <group_name></group_name>
      </tab>
      <tab>
        <name>B (RF fwd)</name>
        <file>channel.bob</file>
        <macros>
          <CH>B</CH>
          <UNIT>mV</UNIT>
        </macros>
        <group_name></group_name>
      </tab>
      <tab>
        <name>C ($(Channel-C))</name>
        <file>channel.bob</file>
        <macros>
          <CH>C</CH>
          <UNIT>V</UNIT>
        </macros>
        <group_name></group_name>
      </tab>
      <tab>
        <name>D ($(Channel-D))</name>
        <file>channel.bob</file>
        <macros>
          <CH>D</CH>
          <UNIT>V</UNIT>
        </macros>
        <group_name></group_name>
      </tab>
      <tab>
        <name>E ($(Channel-E))</name>
        <file>channel.bob</file>
        <macros>
          <CH>E</CH>
          <UNIT>V</UNIT>
        </macros>
        <group_name></group_name>
      </tab>
      <tab>
        <name>F ($(Channel-F))</name>
        <file>channel.bob</file>
        <macros>
          <CH>F</CH>
          <UNIT>V</UNIT>
        </macros>
        <group_name></group_name>
      </tab>
      <tab>
        <name>G ($(Channel-G))</name>
        <file>channel.bob</file>
        <macros>
          <CH>G</CH>
          <UNIT>V</UNIT>
        </macros>
        <group_name></group_name>
      </tab>
      <tab>
        <name>H ($(Channel-H))</name>
        <file>channel.bob</file>
        <macros>
          <CH>H</CH>
          <UNIT>V</UNIT>
        </macros>
        <group_name></group_name>
      </tab>
    </tabs>
    <x>20</x>
    <y>70</y>
    <width>1060</width>
    <height>220</height>
    <direction>0</direction>
    <tab_width>110</tab_width>
    <tab_spacing>5</tab_spacing>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-background</name>
    <x>20</x>
    <y>300</y>
    <width>1060</width>
    <height>810</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>HEADER2</class>
    <text>Scope Settings</text>
    <x>30</x>
    <y>300</y>
    <width>1040</width>
    <height>50</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="xyplot" version="3.0.0">
    <name>X/Y Plot</name>
    <x>30</x>
    <y>450</y>
    <width>1040</width>
    <height>650</height>
    <scripts>
      <script file="EmbeddedPy" check_connections="false">
        <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

sampling	= PVUtil.getDouble(pvs[0])
pre_trigger 	= PVUtil.getDouble(pvs[1])
t_offset		= -pre_trigger*(1/sampling)*1e6

# Channel-A
widget.setPropertyValue('traces[0].x_pv', "=arraySum('"+str(pvs[2])+"', "+str(t_offset)+")")
widget.setPropertyValue('traces[0].y_pv', "=arrayDivScalar('"+str(pvs[3])+"',1)")

# Channel-B
widget.setPropertyValue('traces[1].x_pv', "=arraySum('"+str(pvs[4])+"', "+str(t_offset)+")")
widget.setPropertyValue('traces[1].y_pv', "=arrayDivScalar('"+str(pvs[5])+"',1)")

# Channel-C
widget.setPropertyValue('traces[2].x_pv', "=arraySum('"+str(pvs[6])+"', "+str(t_offset)+")")
widget.setPropertyValue('traces[2].y_pv', "=arrayDivScalar('"+str(pvs[7])+"',1)")

#Channel-D
widget.setPropertyValue('traces[3].x_pv', "=arraySum('"+str(pvs[8])+"', "+str(t_offset)+")")
widget.setPropertyValue('traces[3].y_pv', "=arrayDivScalar('"+str(pvs[9])+"',1)")

#Channel-E
widget.setPropertyValue('traces[4].x_pv', "=arraySum('"+str(pvs[10])+"', "+str(t_offset)+")")
widget.setPropertyValue('traces[4].y_pv', "=arrayDivScalar('"+str(pvs[11])+"',1)")

#Channel-F
widget.setPropertyValue('traces[5].x_pv', "=arraySum('"+str(pvs[12])+"', "+str(t_offset)+")")
widget.setPropertyValue('traces[5].y_pv', "=arrayDivScalar('"+str(pvs[13])+"',1)")

#Channel-G
widget.setPropertyValue('traces[6].x_pv', "=arraySum('"+str(pvs[14])+"', "+str(t_offset)+")")
widget.setPropertyValue('traces[6].y_pv', "=arrayDivScalar('"+str(pvs[15])+"',1)")

#Channel-H
widget.setPropertyValue('traces[7].x_pv', "=arraySum('"+str(pvs[16])+"', "+str(t_offset)+")")
widget.setPropertyValue('traces[7].y_pv', "=arrayDivScalar('"+str(pvs[17])+"',1)")]]></text>
        <pv_name>$(P)$(R)SamplingFreq</pv_name>
        <pv_name>$(P)$(R)NumPreTrigSamples</pv_name>
        <pv_name trigger="false">$(P)$(R)A-TRC2ArrayTimeUs</pv_name>
        <pv_name trigger="false">$(P)$(R)A-TRC2ArrayData</pv_name>
        <pv_name trigger="false">$(P)$(R)B-TRC2ArrayTimeUs</pv_name>
        <pv_name trigger="false">$(P)$(R)B-TRC2ArrayData</pv_name>
        <pv_name trigger="false">$(P)$(R)C-TRC2ArrayTimeUs</pv_name>
        <pv_name trigger="false">$(P)$(R)C-TRC2ArrayData</pv_name>
        <pv_name trigger="false">$(P)$(R)D-TRC2ArrayTimeUs</pv_name>
        <pv_name trigger="false">$(P)$(R)D-TRC2ArrayData</pv_name>
        <pv_name trigger="false">$(P)$(R)E-TRC2ArrayTimeUs</pv_name>
        <pv_name trigger="false">$(P)$(R)E-TRC2ArrayData</pv_name>
        <pv_name trigger="false">$(P)$(R)F-TRC2ArrayTimeUs</pv_name>
        <pv_name trigger="false">$(P)$(R)F-TRC2ArrayData</pv_name>
        <pv_name trigger="false">$(P)$(R)G-TRC2ArrayTimeUs</pv_name>
        <pv_name trigger="false">$(P)$(R)G-TRC2ArrayData</pv_name>
        <pv_name trigger="false">$(P)$(R)H-TRC2ArrayTimeUs</pv_name>
        <pv_name trigger="false">$(P)$(R)H-TRC2ArrayData</pv_name>
      </script>
      <script file="EmbeddedPy" check_connections="false">
        <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil
#AD
ad_min = PVUtil.getDouble(pvs[0])
ad_max = PVUtil.getDouble(pvs[1])
widget.setPropertyValue('y_axes[2].minimum', ad_min)
widget.setPropertyValue('y_axes[2].maximum', ad_max)

#EPU
epu_min = PVUtil.getDouble(pvs[2])
epu_max = PVUtil.getDouble(pvs[3])
widget.setPropertyValue('y_axes[3].minimum', epu_min)
widget.setPropertyValue('y_axes[3].maximum', epu_max)]]></text>
        <pv_name>loc://$(R)ADmin</pv_name>
        <pv_name>loc://$(R)ADmax</pv_name>
        <pv_name>loc://$(R)EPUmin</pv_name>
        <pv_name>loc://$(R)EPUmax</pv_name>
      </script>
    </scripts>
    <x_axis>
      <title>Time [us]</title>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
      <minimum>0.0</minimum>
      <maximum>10.0</maximum>
      <show_grid>true</show_grid>
      <title_font>
        <font family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </title_font>
      <scale_font>
        <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </scale_font>
      <visible>true</visible>
    </x_axis>
    <y_axes>
      <y_axis>
        <title>Trigger [V]</title>
        <autoscale>false</autoscale>
        <log_scale>false</log_scale>
        <minimum>-1.0</minimum>
        <maximum>10.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <on_right>false</on_right>
        <visible>true</visible>
        <color>
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </color>
      </y_axis>
      <y_axis>
        <title>RF power [mV]</title>
        <autoscale>false</autoscale>
        <log_scale>false</log_scale>
        <minimum>-60.0</minimum>
        <maximum>20.0</maximum>
        <show_grid>false</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <on_right>false</on_right>
        <visible>true</visible>
        <color>
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </color>
      </y_axis>
      <y_axis>
        <title>Arc [V]</title>
        <autoscale>false</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>0.5</maximum>
        <show_grid>true</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <on_right>false</on_right>
        <visible>true</visible>
        <color>
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </color>
      </y_axis>
      <y_axis>
        <title>EPU [V]</title>
        <autoscale>false</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>0.5</maximum>
        <show_grid>true</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <on_right>false</on_right>
        <visible>true</visible>
        <color>
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </color>
      </y_axis>
    </y_axes>
    <traces>
      <trace>
        <name>Trigger</name>
        <x_pv></x_pv>
        <y_pv></y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color name="BLACK" red="0" green="0" blue="0">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>RF Fwd</name>
        <x_pv></x_pv>
        <y_pv></y_pv>
        <err_pv></err_pv>
        <axis>1</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="228" green="26" blue="28">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>$(Channel-C)</name>
        <x_pv></x_pv>
        <y_pv></y_pv>
        <err_pv></err_pv>
        <axis>2</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="55" green="126" blue="184">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>$(Channel-D)</name>
        <x_pv></x_pv>
        <y_pv></y_pv>
        <err_pv></err_pv>
        <axis>2</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="77" green="175" blue="74">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>$(Channel-E)</name>
        <x_pv></x_pv>
        <y_pv></y_pv>
        <err_pv></err_pv>
        <axis>2</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="152" green="78" blue="163">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>$(Channel-F)</name>
        <x_pv></x_pv>
        <y_pv></y_pv>
        <err_pv></err_pv>
        <axis>2</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="255" green="127" blue="0">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>$(Channel-G)</name>
        <x_pv></x_pv>
        <y_pv></y_pv>
        <err_pv></err_pv>
        <axis>3</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="166" green="86" blue="40">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>$(Channel-H)</name>
        <x_pv></x_pv>
        <y_pv></y_pv>
        <err_pv></err_pv>
        <axis>3</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="247" green="129" blue="191">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
    </traces>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-background</name>
    <x>30</x>
    <y>350</y>
    <width>1040</width>
    <height>90</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="BACKGROUND" red="220" green="225" blue="221">
      </color>
    </background_color>
    <corner_width>5</corner_width>
    <corner_height>5</corner_height>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="execute">
        <script file="EmbeddedPy">
          <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('y_axes[0].visible', False)
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[0].visible', False)]]></text>
        </script>
        <description>hide Trigger</description>
      </action>
    </actions>
    <x>50</x>
    <y>400</y>
    <width>150</width>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="execute">
        <script file="EmbeddedPy">
          <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('y_axes[0].visible', True)
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[0].visible', True)]]></text>
        </script>
        <description>show Trigger</description>
      </action>
    </actions>
    <x>50</x>
    <y>360</y>
    <width>150</width>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="execute">
        <script file="EmbeddedPy">
          <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[2].visible', True)
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[3].visible', True)
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[6].visible', True)]]></text>
        </script>
        <description>show Cavity$(C1)</description>
      </action>
    </actions>
    <x>230</x>
    <y>360</y>
    <width>150</width>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="execute">
        <script file="EmbeddedPy">
          <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[2].visible', False)
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[3].visible', False)
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[6].visible', False)]]></text>
        </script>
        <description>hide Cavity$(C1)</description>
      </action>
    </actions>
    <x>230</x>
    <y>400</y>
    <width>150</width>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="execute">
        <script file="EmbeddedPy">
          <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[4].visible', True)
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[5].visible', True)
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[7].visible', True)]]></text>
        </script>
        <description>show Cavity$(C2)</description>
      </action>
    </actions>
    <x>410</x>
    <y>360</y>
    <width>150</width>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions execute_as_one="true">
      <action type="execute">
        <script file="EmbeddedPy">
          <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[4].visible', False)
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[5].visible', False)
ScriptUtil.findWidgetByName(widget,"X/Y Plot").setPropertyValue('traces[7].visible', False)]]></text>
        </script>
        <description>hide Cavity$(C2)</description>
      </action>
    </actions>
    <x>410</x>
    <y>400</y>
    <width>150</width>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>Arc Max:</text>
    <x>620</x>
    <y>400</y>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>Arc Min:</text>
    <x>620</x>
    <y>360</y>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="textentry" version="3.0.0">
    <name>Text Entry</name>
    <pv_name>loc://$(R)ADmax(1)</pv_name>
    <x>730</x>
    <y>400</y>
    <height>30</height>
    <horizontal_alignment>1</horizontal_alignment>
  </widget>
  <widget type="textentry" version="3.0.0">
    <name>Text Entry</name>
    <pv_name>loc://$(R)ADmin(0)</pv_name>
    <x>730</x>
    <y>360</y>
    <height>30</height>
    <horizontal_alignment>1</horizontal_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>EPU Min:</text>
    <x>840</x>
    <y>360</y>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>CAPTION</class>
    <text>EPU Max:</text>
    <x>840</x>
    <y>400</y>
    <height>30</height>
    <foreground_color use_class="true">
      <color name="Text" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <horizontal_alignment use_class="true">2</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
  </widget>
  <widget type="textentry" version="3.0.0">
    <name>Text Entry</name>
    <pv_name>loc://$(R)EPUmin(0.3)</pv_name>
    <x>950</x>
    <y>360</y>
    <height>30</height>
    <horizontal_alignment>1</horizontal_alignment>
  </widget>
  <widget type="textentry" version="3.0.0">
    <name>Text Entry</name>
    <pv_name>loc://$(R)EPUmax(1.3)</pv_name>
    <x>950</x>
    <y>400</y>
    <height>30</height>
    <horizontal_alignment>1</horizontal_alignment>
  </widget>
</display>
